package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Activity {
    public static void main(String[] args){

// ARRAY
        String[] fruits = {"apple", "avocado", "banana", "kiwi", "orange"};

        Scanner fruitsScanner = new Scanner(System.in);

        System.out.println(Arrays.toString(fruits));
        System.out.println("Which fruit would you like to get the index of?");

        String fruit = fruitsScanner.nextLine();
        int index = Arrays.binarySearch(fruits,fruit);
        System.out.println("The index of " + fruit + " is: "+ index);

// ARRAY LIST
        ArrayList<String> friends = new ArrayList<>(Arrays.asList("Ellie","Mics","Lea","Meyo"));
        System.out.println("My friends are: " + friends);

// HASHMAP
        HashMap<String,Integer> inventory = new HashMap<>();
        inventory.put("toothpaste",15);
        inventory.put("toothbrush",20);
        inventory.put("soap",12);

        System.out.println("Our current inventory consists of:");
        System.out.println(inventory);

    }
}
