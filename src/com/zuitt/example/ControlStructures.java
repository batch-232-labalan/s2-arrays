package com.zuitt.example;

import java.util.Scanner;

public class ControlStructures {
    public static void main(String[] args) {

        int num1 = 15;
        if(num1 % 5 == 0){
            System.out.println(num1 + " is divisible by 5");
        } else {
            System.out.println(num1 + " is not divisible by 5");
        }

        num1 = 36;
        if(num1 % 5 == 0){
            System.out.println(num1 + " is divisible by 5");
        } else {
            System.out.println(num1 + " is not divisible by 5");
        }

        Scanner numberScanner = new Scanner(System.in);

        System.out.println("Enter a number:");
//        int number = numberScanner.nextInt();
//
//        if(number % 2 == 0){
//            System.out.println(number + " is even!");
//        }else{
//            System.out.println(number + " is odd!");
//        }

        //short-circuiting
//        int x = 15;
//        int y = 0;
//        if(y == 0 || x == 15 || y != 0) System.out.println(true);

        int x = 19;
        int y = 5;
        if(y == 0 || x == 15) {
            System.out.println(true);
        } else {
            System.out.println(false);
        }


        //ternary operator
        int num2 = 24;
        String result = (num2 > 0) ? Boolean.toString(true) : Boolean.toString(false);
        System.out.println(result);


        //Switch Cases
        System.out.println("Enter a number from 1-4 to see a SM Mall on one of the four main SM malls");
        int directionValue = numberScanner.nextInt();

        switch(directionValue){

            case 1:
                System.out.println("SM North Edsa");
                break;
            case 2:
                System.out.println("SM Southmall");
                break;
            case 3:
                System.out.println("SM City Taytay");
                break;
            case 4:
                System.out.println("SM Manila");
                break;
            default:
                System.out.println("Out of Range.");
        }

    }
}
