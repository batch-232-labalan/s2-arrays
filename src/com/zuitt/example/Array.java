package com.zuitt.example;

import java.util.ArrayList;
import  java.util.Arrays;
import  java.util.Collections;
import java.util.HashMap;

public class Array {
    public static void main(String[] args){
          /*
        Arrays in Java
        In Java, arrays are significantly rigid(strict), even before filling
        in the arrays we should already identify the data type and the size of the array.

        Syntax:
        dataType[] identifier = new dataType[numberOfElements]
        dataType[] identifier = {elementA, elementB, elementC, ...}
     */

        String[] newArr = new String[3];
        System.out.println(Arrays.toString(newArr)); //res: [null, null, null]
        System.out.println(newArr); //memory address of the array res: [Ljava.lang.String;@372f7a8d

        newArr[0] = "Clark";
        newArr[1] = "Bruce";
        newArr[2] ="Lois";

        //newArr[3] = "Barry"; //res: error (only 3 strings are initialized)
        //newArr[3] = 25; res: not compatible data types
        System.out.println(Arrays.toString(newArr));

        //=================STRINGS===================
        String[] arrSample = {"Tony", "Steve", "Thor", ""};
        arrSample[3] = "Peter"; // from: ""
        //arrSample[4] = "James"; RES: error
        System.out.println(Arrays.toString(arrSample));


//=======================INTEGERS=================
        Integer[] intArr = new Integer[5];
        intArr[0] = 54;
        intArr[1] = 23;
        intArr[2] = 25;
        intArr[3] = 30;
        intArr[4] = 62;

        System.out.println("Initial order of the intArr: " + Arrays.toString(intArr)); //Res: [54, 23, 25, 30, 62]

        //ASCENDING
        Arrays.sort(intArr); //Res: [23, 25, 30, 54, 62]
        System.out.println("Initial order of the intArr: " + Arrays.toString(intArr));

        //DESCENDING
        Arrays.sort(intArr, Collections.reverseOrder()); //Res: [62, 54, 30, 25, 23] => imported from collections util
        System.out.println("Initial order of the intArr: " + Arrays.toString(intArr));


 //=============ARRAY LIST==============
        /*
           Array lists are resizable collections
           Syntax: ArrayList<dataType> identifier = new ArrayList<>();
         */

        ArrayList<String> students = new ArrayList<>();
        //Array List methods
        //arrayListName.add(itemToAdd) - adds elements in our array list

        students.add("Paul");
        students.add("John");
        System.out.println(students);

        //arrayListName.get(index) - retrieve items from the array list
        System.out.println(students.get(1)); //Res: John

        //arrayListName.set(index, value) - update an item by its index
        students.set(0, "George"); //RES : paul ---> george
        System.out.println(students);

        //arrayListName.remove(index) - removes an item by its index
        students.remove(1);
        System.out.println(students);

        //arrayListName.clear() - clears out items in the array list
        students.clear();
        System.out.println(students);

        //arrayListName.size(); - gets the length of our array list
        students.add("Wanda");
        students.add("Captain Marvel");
        students.add("Wasp");
        System.out.println(students.size()); //Res: 3


 //========ArrayList with initialized values=======
        ArrayList<String> employees = new ArrayList<>(Arrays.asList("Bill Gates", "Elon Mask", "Jeff Bezos"));
        System.out.println(employees);

        employees.add("Lucio Tan"); // can add in array list
        System.out.println(employees);

        //add(index, elementToBeAdded)
        employees.add( 0, "Henry Sy");
        System.out.println(employees);

//=========Hashmaps==========
        //in Java "keys" also referred as "fields"
        //Syntax: HashMap<fieldDataType, valueDataType> identifier = new Hashmap

        HashMap<String, String> userRoles = new HashMap<>();
        //add new field and values in the HashMap
        //hashMapName.put(<field>, <value>);

        userRoles.put("Anna", "Admin");
        userRoles.put("Alice", "Mage");
        System.out.println(userRoles);

        //update
        userRoles.put("Alice", "Marksman"); //res: mage ---> marksman
        userRoles.put("Dennis", "Tank");
        System.out.println(userRoles);

        // retrieve values by fields
        System.out.println(userRoles.get("Alice"));
        System.out.println(userRoles.get("Anna"));
        System.out.println(userRoles.get("Dennis"));


        //removes an element / field value
        //hashMapName.remove("field");

        userRoles.remove("Anna");
        System.out.println(userRoles);

        //retrieve hashMap keys
        //hashMapName.keySet();
        System.out.println(userRoles.keySet());

    }
}
